Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

	The logger.log is printed to by different commands than the console. Thus, there is different output printed to it than the console.


2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

	This is a junit test which checks for a disable condition and returns true if it is not found.


3.  What does Assertions.assertThrows do?

	Assertions.assertThrows will check to ensure that the tested application throws the correct exception.


4.  See TimerException and there are 3 questions

	1.  What is serialVersionUID and why do we need it? (please read on Internet)

		It is necessary to ensure that the user has the correct version of a class loaded; used in applications where files may be downloaded from the internet and stored locally, and thus become outdated eventually.


	2.  Why do we need to override constructors?

		It is important to ensure that the exception is registered as the correct type to JUnit after it is constructed.


	3.  Why we did not override other Exception methods?	

		They are not going to be tested by JUnit.


5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

	It gets the config file for the logger and sets it up, or prints an error and does not configure the logger if the needed file is missing.


6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

	It is a file in Markdown syntax, which is used by bitbucket server to format text.


7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

	The timeNow is never reassigned from null when the TimerException is raised. Then, we try to subtract null from a value and throw a nullpointer exception.


8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

	Since the first exception thrown goes to the finally{} block, there is still an opportunity for the nullptr exception to arise.


9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

	![Check pics for JUNIT.png](\pics\JUNIT.png)


10.  Make a printScreen of your eclipse Maven test run, with console

	![Check pics for MAVEN.png](\pics\MAVEN.png)


11.  What category of Exceptions is TimerException and what is NullPointerException

	TimerException is an Exception, while NullPointerException is a RuntimeException, which extends Exception.


12.  Push the updated/fixed source code to your own repository.

    https://bitbucket.org/JR_Diehl/altogether-05/src/master/
	
